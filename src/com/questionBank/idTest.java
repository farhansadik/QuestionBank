package com.questionBank;

import java.io.*;
import java.util.Scanner;

public class idTest {
    public static void main(String[] args) {

        // creating files
        Scanner input = new Scanner(System.in);
        System.out.print("enter file name ~ ");
        String name = input.nextLine();
        File myFile = new File(name + ".txt");

        try {
            myFile.createNewFile();
        } catch (IOException a) {
            System.out.println(a);
        }

        // writing file
        try{
            FileWriter fw = new FileWriter(myFile);

            fw.write("Name : Nazmul Islam\n");
            fw.write("Name : Mahmudul Hasan\n");
            fw.write("Name : AK Azad\n");
            fw.write("Name : Dumb Ass\n");

            fw.flush();
            fw.close();
            System.out.println("name has been added successfully!");
        } catch (Exception e){
            System.err.println("Error while writing to file: " + e.getMessage());
        }


        try {
            FileWriter pop = new FileWriter(myFile,true);
            BufferedWriter out = new BufferedWriter(pop);

            System.out.print("y/n ~ ");
            String q1 = input.nextLine();
            out.write(q1);

            System.out.print("y/n ~ ");
            String q2 = input.nextLine();
            out.write(q2);

            out.close();
        } catch (Exception e){
            System.err.println("Error while writing to file: " + e.getMessage());
        }
    }
}
