package com.questionBank;

import java.awt.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.io.*;

public class Main {
    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        Main menuList = new Main();
        menuList.MainMenu();
        int inputValue = input.nextInt();

        // calling function from inner class
        //Main.TestClass1 myObject = new Main().new TestClass1();
        //myObject.MainMenu();

        if (inputValue == 1) {
            // Question Bank Menu
            menuList.QuestionBankMenu();
            int inputQM = input.nextInt();
            if ( inputQM == 1 ){

                Main.GetQuestion myObject = new Main().new GetQuestion();
                Scanner qsn = new Scanner(System.in);

                // load questions
                System.out.println("Currently your question banks in empty.");
                System.out.print("Please enter the question banks file name: ");
                String loadQsn = qsn.nextLine();
                try {
                    // calling function from inner class
                    String data = myObject.readFileAsString(loadQsn);
                    System.out.println(data);
                } catch (IOException e) {
                    System.out.println("File not found");
                }

            } else if (inputQM == 2) {
                // display question details
                // 3 questions per page
                Main.GetQuestion myObject = new Main().new GetQuestion();
                myObject.showQuestionsDetails();
            } else if (inputQM == 3) {
                System.exit(0);
            } else {
                System.out.println("Invalid Options");
            }
        } else if (inputValue == 2 ) {
            menuList.StudentMaintananceMenu();
            Scanner mainmenu = new Scanner(System.in);
            int mainMenu = mainmenu.nextInt();

            switch (mainMenu) {
                case 1:
                    // show details
                    Scanner ViewDetails = new Scanner(System.in);
                    System.out.print("enter the id code ~ ");
                    String vd = ViewDetails.nextLine();
                    File file = new File(vd+".txt");
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String st;
                    while ((st = br.readLine()) != null)
                        System.out.println(st);
                    break;
                case 2:
                    // add account
                    Scanner addACC = new Scanner(System.in);
                    System.out.print("Please enter ID Name ~ ");
                    String addAc = addACC.nextLine();
                    File myFile = new File(addAc + ".txt");

                    // creating files
                    try {
                        myFile.createNewFile();

                    } catch (FileNotFoundException e) {
                        System.out.println("File Not Found" + e);
                    } catch (IOException a) {
                        System.out.println(a);
                    }

                    // writing file
                    System.out.print("enter student name : ");
                    String name = addACC.nextLine();
                    System.out.println("student ID         : " + addAc);

                    FileWriter fileWriter = new FileWriter(myFile);

                    fileWriter.write(name + "\n");
                    fileWriter.close();

                    System.out.println("The Account Has been Created");
                    break;
                case 3:
                    // update account
                    Scanner updateA = new Scanner(System.in);
                    System.out.print("Please enter ID Name ~ ");
                    String UpdateAcc = updateA.nextLine();

                    //text file, should be opening in default text editor
                    File ua = new File(UpdateAcc + ".txt");

                    //first check if Desktop is supported by Platform or not
                    if(!Desktop.isDesktopSupported()){
                        System.out.println("Desktop is not supported");
                        return;
                    }

                    Desktop desktop = Desktop.getDesktop();
                    if (ua.exists()) desktop.open(ua);

                    break;
                case 4:
                    // delete account
                    Scanner deleteAcc = new Scanner(System.in);
                    System.out.print("Please enter ID Name ~ ");
                    String DeleteAcC = deleteAcc.nextLine();
                    File da = new File(DeleteAcC + ".txt");

                    if (da.delete()) {
                        System.out.println("File deleted successfully");
                    } else {
                        System.out.println("Failed to delete the file");
                    }

                    break;
                case 5:
                    System.exit(0);
                default:
                    System.out.println("Invalid Options");
            }

        } else if (inputValue == 3 ) {
            // Examination Menu
            Scanner exam = new Scanner(System.in);
            menuList.ExaminationMenu();
            int mainMenu = exam.nextInt();

            switch (mainMenu) {
                case 1:
                    Scanner ViewDetails = new Scanner(System.in);
                    System.out.print("enter the id code ~ ");
                    String vd = ViewDetails.nextLine();
                    File file = new File(vd+".txt");
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String st;
                    while ((st = br.readLine()) != null)
                        System.out.println(st);
                    break;
                case 2:
                    // checking student ID
                    Scanner cE = new Scanner(System.in);

                    System.out.print("Enter your student ID: ");
                    String data = cE.nextLine();
                    File checkFile = new File(data + ".txt");
                    BufferedReader brceS = new BufferedReader(new FileReader(checkFile));
                    String stC;
                    while ((stC = brceS.readLine()) != null)
                        System.out.println("Welcome " + stC);

                    // creating files

                    //File myFile = new File(data + "-studentTests.txt");
                    File myFile = new File("studentTests.txt");
                    try {
                        myFile.createNewFile();
                    } catch (IOException a) {
                        System.out.println(a);
                    }

                    // writing files
                    FileWriter out = new FileWriter(myFile, true);
                    BufferedWriter fileWriter = new BufferedWriter(out);
                    Main.ExaminationClass myObject = new Main().new ExaminationClass();
                    fileWriter.write("\n" + data + "\n");
                    /*
                    File q1 = new File("q1.txt");
                    BufferedReader br1 = new BufferedReader(new FileReader(q1));
                    String st1;
                    while ((st1 = br1.readLine()) != null)
                        System.out.println(st1);
                    System.out.print("Enter your answer: ");
                    String sq1 = cE.nextLine();
                    fileWriter.write(sq1);

                    File q2 = new File("q2.txt");
                    BufferedReader br2 = new BufferedReader(new FileReader(q2));
                    String st2;
                    while ((st2 = br2.readLine()) != null)
                        System.out.println(st2);
                    System.out.print("Enter your answer: ");
                    String sq2 = cE.nextLine();
                    fileWriter.write(sq2);
                    */

                    System.out.println("\nQuestion 1/10\n");
                    myObject.getQuestion1();
                    System.out.print("Enter your answer: ");
                    String q1 = cE.nextLine();
                    fileWriter.write(q1);

                    System.out.println("\nQuestion 2/10\n");
                    myObject.getQuestion2();
                    System.out.print("Enter your answer: ");
                    String q2 = cE.nextLine();
                    fileWriter.write(q2);

                    System.out.println("\nQuestion 3/10\n");
                    myObject.getQuestion3();
                    System.out.print("Enter your answer: ");
                    String q3 = cE.nextLine();
                    fileWriter.write(q3);

                    System.out.println("\nQuestion 4/10\n");
                    myObject.getQuestion4();
                    System.out.print("Enter your answer: ");
                    String q4 = cE.nextLine();
                    fileWriter.write(q4);

                    System.out.println("\nQuestion 5/10\n");
                    myObject.getQuestion5();
                    System.out.print("Enter your answer: ");
                    String q5 = cE.nextLine();
                    fileWriter.write(q5);

                    System.out.println("\nQuestion 6/10\n");
                    myObject.getQuestion6();
                    System.out.print("Enter your answer: ");
                    String q6 = cE.nextLine();
                    fileWriter.write(q6);

                    System.out.println("\nQuestion 7/10\n");
                    myObject.getQuestion7();
                    System.out.print("Enter your answer: ");
                    String q7 = cE.nextLine();
                    fileWriter.write(q7);

                    System.out.println("\nQuestion 8/10\n");
                    myObject.getQuestion8();
                    System.out.print("Enter your answer: ");
                    String q8 = cE.nextLine();
                    fileWriter.write(q8);

                    System.out.println("\nQuestion 9/10\n");
                    myObject.getQuestion9();
                    System.out.print("Enter your answer: ");
                    String q9 = cE.nextLine();
                    fileWriter.write(q9);

                    System.out.println("\nQuestion 10/10\n");
                    myObject.getQuestion10();
                    System.out.print("Enter your answer: ");
                    String q10 = cE.nextLine();
                    fileWriter.write(q10);

                    fileWriter.close();
                    break;
                case 3:
                    // export result
                    // printing date
                    Date date = new Date();
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    String strDate = formatter.format(date);
                    // System.out.println("Java Programming Lab Test\t\t " + strDate);

                    File source = new File("studentTests.txt");
                    File dest = new File("exam.txt");

                    Main obj = new Main();
                    String header = "Java Programming Lab Test\t\t " + strDate;

                    obj.copyFileUsingJava7Files(source, dest);
                    System.out.println("File Copyed Succefully");

                    FileWriter exportWriter = new FileWriter(dest, true);
                    BufferedWriter bwWriter = new BufferedWriter(exportWriter);
                    bwWriter.write("\n" + header);
                    bwWriter.close();
                    System.out.println("File writed Succefully");

                    break;
                case 4:
                    // process result
                    System.out.println("case 4");
                    break;
                case 5:
                    System.exit(0);
                default:
                    System.out.println("Invalid Options");
            }
        } else if (inputValue == 4 ) {
            System.exit(0);
        } else {
            System.out.println("Invalid Options");
        }
    }
    private void copyFileUsingJava7Files(File source, File dest) throws IOException {
        // coiying file
        Files.copy(source.toPath(), dest.toPath());
    }
    private void MainMenu() {
        System.out.println("Lab Tests Automation System\n");
        System.out.println("    1) Question Bank"); // done
        System.out.println("    2) Student Maintenance"); // done
        System.out.println("    3) Examination");
        System.out.println("    4) Exit"); // done
    }
    private void QuestionBankMenu() {
        System.out.println("Question Bank\n");
        System.out.println("    1) Load Questions"); // done
        System.out.println("    2) Display Questions Details"); // done
        System.out.println("    3) Exit"); // done
    }
    private void StudentMaintananceMenu() {
        System.out.println("Student Maintenance\n");
        System.out.println("    1) Show Details"); // done
        System.out.println("    2) Add Account"); // done
        System.out.println("    3) Update Account"); // done
        System.out.println("    4) Delete Account"); // done
        System.out.println("    5) Exit"); // done
    }
    private void ExaminationMenu() {
        System.out.println("Examination\n");
        System.out.println("    1) Create Examination");
        System.out.println("    2) Start Examination"); // done
        System.out.println("    3) Export Results");
        System.out.println("    4) Process Results");
        System.out.println("    5) Exit"); // done
    }
    public class GetQuestion {
        public String readFileAsString(String fileName)throws Exception {
            // static
            String data = "";
            data = new String(Files.readAllBytes(Paths.get(fileName)));
            return data;
        }
        public void showQuestionsDetails() {
            File myFile = new File("questionbanks.txt");
            try {
                FileReader fr = new FileReader(myFile);
                char[] data = new char[371];
                fr.read(data);
                System.out.println(data);
            } catch (IOException e) {
                System.out.println("File not found" + e);
            }
        }
    }
    public class ExaminationClass {
        private void getQuestion1() {
            System.out.println("The computer can do such a wide variety of tasks because:");
            System.out.println("a. It can be programmed");
            System.out.println("b. It is a machine");
            System.out.println("c. It contains a central processing unit (CPU)");
            System.out.println("d. It has the ability to connect to the Internet");
        }
        private void getQuestion2() {
            System.out.println("Computer programming is:");
            System.out.println("a. An art");
            System.out.println("b. A science");
            System.out.println("c. Both of the above");
            System.out.println("d. Neither of the above");
        }
        private void getQuestion3() {
            System.out.println("Another term for programs is:");
            System.out.println("a. Hardware");
            System.out.println("b. Software");
            System.out.println("c. Firmware");
            System.out.println("d. Shareware");
        }
        private void getQuestion4() {
            System.out.println("_________ refers to the physical components that a computer is made of.");
            System.out.println("a. Device");
            System.out.println("b. System");
            System.out.println("c. Hardware");
            System.out.println("d. Software");
        }
        private void getQuestion5() {
            System.out.println("A program is a sequence of instructions stored in:");
            System.out.println("a. The CPU");
            System.out.println("b. The computer's memory");
            System.out.println("c. Software");
            System.out.println("d. Firmware");
        }
        private void getQuestion6() {
            System.out.println("A byte is a collection of:");
            System.out.println("a. Four bits");
            System.out.println("b. Six bits");
            System.out.println("c. Eight bits");
            System.out.println("d. A dollar");
        }
        private void getQuestion7() {
            System.out.println("An operating system can be categorized according to:");
            System.out.println("a. The number of users they can accommodate");
            System.out.println("b. The number of tasks they can perform at one time");
            System.out.println("c. Both of the above");
            System.out.println("d. Neither of the above");
        }
        private void getQuestion8() {
            System.out.println("This is a special language used to write computer programs.");
            System.out.println("a. Programming language");
            System.out.println("b. Operating system");
            System.out.println("c. Application");
            System.out.println("d. Pseudocode");
        }
        private void getQuestion9() {
            System.out.println("The original name for Java was");
            System.out.println("a. Java");
            System.out.println("b. HotJava");
            System.out.println("c. Elm");
            System.out.println("d. Oak");
        }
        private void getQuestion10() {
            System.out.println("Syntax is:");
            System.out.println("a. Words that have a special meaning in the programming language");
            System.out.println("b. Rules that must be followed when writing a program");
            System.out.println("c. Punctuation");
            System.out.println("d. Symbols or words that perform operations");
        }
    }
}
