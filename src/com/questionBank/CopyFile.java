package com.questionBank;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class CopyFile {

    private void copyFileUsingJava7Files(File source, File dest) throws IOException {
        // coiying file
        Files.copy(source.toPath(), dest.toPath());
    }

    public static void main(String[] args) {

        File source = new File("studentTests.txt");
        File dest = new File("a1.txt");

        CopyFile obj = new CopyFile();
        try {
            obj.copyFileUsingJava7Files(source, dest);
        } catch (IOException e) {
            System.out.println(e);
        }

    }

}
